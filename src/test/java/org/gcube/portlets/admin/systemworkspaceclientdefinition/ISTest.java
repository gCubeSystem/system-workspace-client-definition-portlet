package org.gcube.portlets.admin.systemworkspaceclientdefinition;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.admin.systemworkspaceclientdefinition.definition.DefinitionItem;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.is.InformationSystemUtils;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.shared.Constants;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ISTest {

	private static Logger logger = LoggerFactory.getLogger(ISTest.class);

	@Ignore
	@Test
	public void findSD() {

		try {
			InformationSystemUtils.retrieveSD(Constants.SYSTEM_WORKSPACE_CLIENT_CATEGORY, Constants.DEFAULT_SCOPE);
			assertTrue(true);

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			assertTrue("Error searching the resource!", false);
		}

	}

	@Ignore
	public void publishSD() {

		try {
			DefinitionItem definitionItem = new DefinitionItem("sample-client-name", "sample-client-description",
					"sample-client-description", "xxxxx-xxxx-xxx-xxxx");
			List<String> scopes=new ArrayList<>();
			scopes.add(Constants.DEFAULT_SCOPE);
			
			InformationSystemUtils.publishSD(definitionItem, Constants.SYSTEM_WORKSPACE_CLIENT_CATEGORY,
					Constants.DEFAULT_SCOPE);
			assertTrue(true);

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			assertTrue("Error publishing the resource!", false);
		}

	}

	@Ignore
	public void deleteSD() {

		try {
			DefinitionItem definitionItem = new DefinitionItem("sample-client-name", "sample-client-description",
					"sample-client-description", "xxxxx-xxxx-xxx-xxxx");
			InformationSystemUtils.deleteSD(definitionItem, Constants.SYSTEM_WORKSPACE_CLIENT_CATEGORY,
					Constants.DEFAULT_SCOPE);
			assertTrue(true);

		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			assertTrue("Error deleting the resource!", false);
		}

	}

	
	
}
package org.gcube.portlets.admin.systemworkspaceclientdefinition.shared;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class Constants {

	public static final String DEFAULT_USER = "giancarlo.panichi";
	public static final String DEFAULT_SCOPE = "/gcube";
	public static final String DEFAULT_TOKEN = "";
	
	public static final String DEFAULT_ROLE = "OrganizationMember";

	// Session
	public static final String CURR_GROUP_ID = "CURR_GROUP_ID";

	// Social Resource
	public static final String SOCIAL_NETWORKING_NAME = "SocialNetworking";
	public static final String SOCIAL_NETWORKING_CATEGORY = "Portal";

	
	// SSD Services Endpoint
	public static final String SYSTEM_WORKSPACE_CLIENT_CATEGORY = "SystemWorkspaceClient";

	// IAM Services Endpoint
	public static final String IAM_SERVICE_NAME = "IAM";
	public static final String IAM_SERVICE_CATEGORY = "Auth";   
	
	// SLEEP TIME
	public static final int SLEEP_TIME = 5000;
}


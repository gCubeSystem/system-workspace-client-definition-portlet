package org.gcube.portlets.admin.systemworkspaceclientdefinition.event;

import java.util.List;

import org.gcube.portal.event.publisher.lr62.AbstractLR62EventPublisher;
import org.gcube.portal.event.publisher.lr62.PortalEvent;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.definition.DefinitionItem;
import org.json.simple.JSONArray;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class CreateSDEvent extends PortalEvent {

	private static final long serialVersionUID = 1499288552188273747L;

	public class CreateSDEventPublisher extends AbstractLR62EventPublisher {
		public CreateSDEventPublisher() {
			super();
		}
	}

	public CreateSDEventPublisher publisher;

	public static final String NAME = "create_workspace_client";// "create_system_workspaceClient";
	public static final String CLIENT_ID_ENTRY = "client_id";
	// public static final String DESCRIPTION_ENTRY = "description";
	public static final String CLIENT_SECRET_ENTRY = "client_sercret";
	public static final String CLIENT_CONTEXT_LIST = "context_list";

	public CreateSDEvent(DefinitionItem definitionItem) throws Exception {
		super(NAME);
		publisher = new CreateSDEventPublisher();
		setClientId(definitionItem.getClientId());
		// setDescription(definitionItem.getDescription());
		setClientSecret(definitionItem.getSecret());
		setClientContextList(definitionItem.getContextList());
	}

	public void setClientId(String clientId) {
		set(CLIENT_ID_ENTRY, clientId);
	}

	public String getClientId() {
		return (String) get(CLIENT_ID_ENTRY);
	}

	/*
	 * public void setDescription(String description) { set(DESCRIPTION_ENTRY,
	 * description); }
	 * 
	 * public String getDescription() { return (String) get(DESCRIPTION_ENTRY); }
	 */

	public void setClientSecret(String clientSecret) {
		
		set(CLIENT_SECRET_ENTRY, clientSecret);
	}

	public String getClientSecret() {
		return (String) get(CLIENT_SECRET_ENTRY);
	}

	@SuppressWarnings("unchecked")
	public void setClientContextList(List<String> clientContextList) throws Exception {
		if (clientContextList != null) {
			JSONArray contexts= new JSONArray();
			contexts.addAll(clientContextList);
			put(CLIENT_CONTEXT_LIST, clientContextList);
			
		}

	}

	public String getClientContextList() {
		return (String) get(CLIENT_SECRET_ENTRY);
	}

}

package org.gcube.portlets.admin.systemworkspaceclientdefinition;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public enum OperationType {
	
	TableInteraction;
	
	public static OperationType getTypeFromString(String value){
		for(OperationType a:values()){
			if(a.name().compareToIgnoreCase(value)==0){
				return a;
			} 
		}
		return null;
	}
	
}

package org.gcube.portlets.admin.systemworkspaceclientdefinition.definition;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class DefinitionItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private String clientId;
	private String description;
	private String username;
	private String secret;
	private List<String> contextList;

	public DefinitionItem(String clientId) {
		super();
		this.clientId = clientId;
	}

	public DefinitionItem(String clientId, String description, String username, String secret,
			List<String> contextList) {
		super();
		this.clientId = clientId;
		this.description = description;
		this.username = username;
		this.secret = secret;
		this.contextList = contextList;
	}

	public DefinitionItem(String clientId, String description, String username, String secret) {
		super();
		this.clientId = clientId;
		this.description = description;
		this.username = username;
		this.secret = secret;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public List<String> getContextList() {
		return contextList;
	}

	public void setContextList(List<String> contextList) {
		this.contextList = contextList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "DefinitionItem [clientId=" + clientId + ", description=" + description + ", username=" + username
				+ ", secret=" + secret + ", contextList=" + contextList + "]";
	}

}

package org.gcube.portlets.admin.systemworkspaceclientdefinition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.gcube.portlets.admin.systemworkspaceclientdefinition.definition.DefClientIdComparator;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.definition.DefDescriptionComparator;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.definition.DefinitionItem;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.event.CreateSDEvent;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.event.DeleteSDEvent;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.is.InformationSystemUtils;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.shared.Constants;
import org.gcube.portlets.admin.systemworkspaceclientdefinition.util.UserCredentials;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.ServletResponseUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *         Portlet implementation class SystemWorkspaceClientDefinition
 * 
 */
public class SystemWorkspaceClientDefinition extends MVCPortlet {

	private static Logger logger = LoggerFactory.getLogger(SystemWorkspaceClientDefinition.class);

	private void paramsInfo(ActionRequest actionRequest) {
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
		paramsInfo(httpRequest);

	}

	private void paramsInfo(ResourceRequest resourceRequest) {
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(resourceRequest);
		paramsInfo(httpRequest);

	}

	private void paramsInfo(HttpServletRequest httpRequest) {

		HttpServletRequest hsrequest = PortalUtil.getOriginalServletRequest(httpRequest);
		String queryString = hsrequest.getQueryString();
		logger.debug("Query: " + queryString);

		logger.debug("Params Info");
		for (@SuppressWarnings("unchecked")
		Enumeration<String> e = (Enumeration<String>) httpRequest.getParameterNames(); e.hasMoreElements();) {
			String key = e.nextElement();
			String[] v = httpRequest.getParameterValues(key);
			ArrayList<String> values = new ArrayList<String>(Arrays.asList(v));
			StringBuilder parms = new StringBuilder();
			parms.append("Parameter: ");
			parms.append(key);
			parms.append(" - ");
			parms.append(values);
			logger.debug(parms.toString());
		}

	}

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws PortletException, IOException {
		// GroupManager gm = new LiferayGroupManager();
		try {
			UserCredentials userCredentials = SessionUtil.getUserCredentials(renderRequest);
			logger.debug("Render requested from user: {}", userCredentials);

			/*
			 * String mvcPath = renderRequest.getParameter("mvcPath");
			 * 
			 * if (mvcPath == null || mvcPath.isEmpty()) { mvcPath =
			 * "/html/systemworkspaceclientdefinition/view.jsp"; } PortletRequestDispatcher
			 * dispatcher = getPortletContext().getRequestDispatcher(mvcPath);
			 * dispatcher.include(renderRequest, renderResponse);
			 */

			/*
			 * List<GCubeGroup> userGroups =
			 * gm.listGroupsByUser(userCredentials.getUserId()); List<String> userContexts =
			 * new ArrayList<String>(); for (GCubeGroup g : userGroups) { if
			 * (g.getGroupName().equals(PortalContext.getConfiguration().
			 * getInfrastructureName())) { String context =
			 * gm.getInfrastructureScope(g.getGroupId()); userContexts.add(context); } if
			 * (g.getParentGroupId() > 0) { String context =
			 * gm.getInfrastructureScope(g.getGroupId()); userContexts.add(context); } }
			 * renderRequest.setAttribute("userGroups", userGroups);
			 * renderRequest.setAttribute("userContexts", userContexts);
			 */
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
		}
		super.render(renderRequest, renderResponse);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {

		UserCredentials userCredentials;
		try {
			userCredentials = SessionUtil.getUserCredentials(resourceRequest);
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("success", false);
			jsonObject.put("comment", "Error retrieving user credentials: " + e.getLocalizedMessage());
			resourceResponse.getWriter().println(jsonObject);
			super.serveResource(resourceRequest, resourceResponse);
			return;
		}

		paramsInfo(resourceRequest);

		String opType = ParamUtil.getString(resourceRequest, "operationType", null);
		OperationType operationType = OperationType.getTypeFromString(opType);
		if (operationType == null) {
			logger.error("Invalid Operation Type Requested: null");
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("success", false);
			jsonObject.put("comment", "Invalid Operation Type Requested: null");
			resourceResponse.getWriter().println(jsonObject);
			super.serveResource(resourceRequest, resourceResponse);
			return;
		}

		switch (operationType) {
		case TableInteraction:
			tableInteraction(resourceRequest, resourceResponse, userCredentials);
			break;
		default:
			logger.error("Invalid Operation Type Requested: {}", operationType);
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("success", false);
			jsonObject.put("comment", "Invalid Operation Type Requested: " + operationType);
			resourceResponse.getWriter().println(jsonObject);
			super.serveResource(resourceRequest, resourceResponse);
			break;

		}
	}

	private void tableInteraction(ResourceRequest resourceRequest, ResourceResponse resourceResponse,
			UserCredentials userCredentials) throws IOException, PortletException {

		String searchValue = ParamUtil.getString(resourceRequest, "search[value]");
		int start = ParamUtil.getInteger(resourceRequest, "start");
		int length = ParamUtil.getInteger(resourceRequest, "length");
		int draw = ParamUtil.getInteger(resourceRequest, "draw");
		int orderColumn = ParamUtil.getInteger(resourceRequest, "order[0][column]");
		String orderDirection = ParamUtil.getString(resourceRequest, "order[0][dir]");

		listItems(resourceRequest, resourceResponse, userCredentials, start, length, searchValue, draw, orderColumn,
				orderDirection);

	}

	private void listItems(ResourceRequest request, ResourceResponse resourceResponse, UserCredentials userCredentials,
			int start, int length, String searchValue, int draw, int orderColumn, String orderDirection)
			throws IOException, PortletException {
		StringBuilder tableRequested = new StringBuilder();
		tableRequested.append("Table requested [start=");
		tableRequested.append(start);
		tableRequested.append(", lenght=");
		tableRequested.append(length);
		tableRequested.append(", searchValue=");
		tableRequested.append(searchValue);
		tableRequested.append(", draw=");
		tableRequested.append(draw);
		tableRequested.append(", orderColumn=");
		tableRequested.append(orderColumn);
		tableRequested.append(", orderDirection=");
		tableRequested.append(orderDirection);
		tableRequested.append("]");
		logger.debug(tableRequested.toString());

		// Create
		ArrayList<DefinitionItem> itemsList;

		try {
			itemsList = InformationSystemUtils.retrieveSD(Constants.SYSTEM_WORKSPACE_CLIENT_CATEGORY,
					userCredentials.getCurrentScope());
		} catch (Exception e) {
			logger.error("Error retrieving System Workspace Client from IS. {}", e.getLocalizedMessage(), e);
			throw new PortletException("Error retrieving System Workspace Client from IS. " + e.getLocalizedMessage(),
					e);
		}

		int count = itemsList.size();

		// Filter
		ArrayList<DefinitionItem> filteredItemsList = new ArrayList<>();
		if (searchValue != null && !searchValue.isEmpty()) {
			for (DefinitionItem definitionItem : itemsList) {
				if (definitionItem.getClientId().contains(searchValue)) {
					filteredItemsList.add(definitionItem);
				}
			}
			itemsList.clear();
			itemsList.addAll(filteredItemsList);
		}

		int recordsFiltered = itemsList.size();

		// Order
		reorderData(orderColumn, orderDirection, itemsList);

		// Create data Window
		if (start > recordsFiltered) {
			start = recordsFiltered;
		}

		int limit = 0;
		if (length != -1) {
			if (start + length > recordsFiltered) {
				limit = recordsFiltered;
			} else {
				limit = start + length;
			}
		} else {
			limit = recordsFiltered;
		}

		ArrayList<DefinitionItem> windowItemsList = new ArrayList<>();
		for (int i = start; i < limit; i++) {
			windowItemsList.add(itemsList.get(i));
		}

		JSONObject tableData = buildJSONResponse(windowItemsList, start, length, draw, count, recordsFiltered, request);
		ServletResponseUtil.write(PortalUtil.getHttpServletResponse(resourceResponse), tableData.toString());

	}

	private void reorderData(int orderColumn, String orderDirection, ArrayList<DefinitionItem> itemsList) {
		switch (orderColumn) {
		case 0:
			if (orderDirection != null && orderDirection.compareTo("asc") == 0) {
				Collections.sort(itemsList, Collections.reverseOrder(new DefClientIdComparator()));
			} else {
				Collections.sort(itemsList, new DefClientIdComparator());
			}
			break;
		case 1:
			if (orderDirection != null && orderDirection.compareTo("asc") == 0) {
				Collections.sort(itemsList, Collections.reverseOrder(new DefDescriptionComparator()));
			} else {
				Collections.sort(itemsList, new DefDescriptionComparator());
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 
	 * @param itemsList
	 * @param start
	 * @param lenght
	 * @param draw
	 * @param count
	 * @param request
	 * @return the JSON response to the client
	 */
	private JSONObject buildJSONResponse(ArrayList<DefinitionItem> itemsList, int start, int lenght, int draw,
			int count, int recordsFiltered, ResourceRequest request) {
		JSONArray definitionsArray = JSONFactoryUtil.createJSONArray();
		for (DefinitionItem item : itemsList) {
			JSONObject definitionObject = JSONFactoryUtil.createJSONObject();
			definitionObject.put("ClientId", item.getClientId());
			definitionObject.put("Description", item.getDescription());
			definitionsArray.put(definitionObject);
		}

		JSONObject tableData = JSONFactoryUtil.createJSONObject();
		tableData.put("messages", "Read Done!");
		tableData.put("data", definitionsArray);
		tableData.put("start", start);
		tableData.put("lenght", lenght);
		tableData.put("draw", draw);
		tableData.put("recordsTotal", count);
		tableData.put("recordsFiltered", recordsFiltered);
		logger.debug("tableData:" + tableData.toString());
		return tableData;
	}

	public void createDefinition(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		UserCredentials userCredentials;

		try {
			userCredentials = SessionUtil.getUserCredentials(actionRequest);
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			// actionResponse.addProperty("message", "Error!");
			SessionErrors.add(actionRequest, "error-session");
			SessionMessages.add(actionRequest,
					PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			return;
		}

		paramsInfo(actionRequest);

		String act = ParamUtil.getString(actionRequest, "act");
		if (act == null) {
			SessionErrors.add(actionRequest, "error-create-definition");
			SessionMessages.add(actionRequest,
					PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			return;
		}
		switch (act) {
		case "submit":
			logger.debug("Submit Request");
			String clientId = ParamUtil.getString(actionRequest, "clientId");
			if (clientId == null || clientId.isEmpty()) {
				logger.error("Invalid client id: " + clientId);
				SessionErrors.add(actionRequest, "error-invalid-clientId");
				SessionMessages.add(actionRequest,
						PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/sd_create.jsp");
				break;
			} else {
				if (clientId.contains(" ")) {
					logger.error("Invalid client id: " + clientId);
					SessionErrors.add(actionRequest, "error-invalid-clientId-space");
					SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest)
							+ SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
					PortalUtil.copyRequestParameters(actionRequest, actionResponse);
					actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/sd_create.jsp");
					break;
				}
			}
			String description = ParamUtil.getString(actionRequest, "description");
			if (description == null || description.isEmpty()) {
				logger.error("Invalid description: " + description);
				SessionErrors.add(actionRequest, "error-invalid-description");
				SessionMessages.add(actionRequest,
						PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				PortalUtil.copyRequestParameters(actionRequest, actionResponse);
				actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/sd_create.jsp");
				break;
			}

			logger.debug("Requested: [clientId={}, description={}]", clientId, description);
			try {
				String username = clientId;
				String secret = UUID.randomUUID().toString();
				List<String> scopes=retrieveScopes(userCredentials);
				DefinitionItem definitionItem = new DefinitionItem(clientId, description, username, secret,scopes);
				logger.debug("DefinitionItem: {}",definitionItem);
				InformationSystemUtils.publishSD(definitionItem, Constants.SYSTEM_WORKSPACE_CLIENT_CATEGORY,
						userCredentials.getCurrentScope());
				publishCreateSDEvent(definitionItem);
				SessionMessages.add(actionRequest, "success-definition-created");
				actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			} catch (Exception e) {
				logger.error(e.getLocalizedMessage(), e);
				SessionErrors.add(actionRequest, "error-create-definition");
				SessionMessages.add(actionRequest,
						PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");

			}
			break;
		case "cancel":
			logger.debug("Cancel Request");
			SessionMessages.clear(actionRequest);
			actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			break;
		default:
			logger.debug("Default");
			SessionErrors.add(actionRequest, "error-create-definition");
			SessionMessages.add(actionRequest,
					PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			break;
		}

	}

	private List<String> retrieveScopes(UserCredentials userCredentials) throws Exception {
		List<String> scopes=new ArrayList<>();
		scopes.add(userCredentials.getCurrentScope());
		
		LiferayGroupManager groupManagement = new LiferayGroupManager();
		long idScope=groupManagement.getGroupIdFromInfrastructureScope(userCredentials.getCurrentScope());
		
		Boolean isRoot = groupManagement
				.isRootVO(idScope);
		if(isRoot) {
			GCubeGroup gCubeG=groupManagement.getGroup(idScope);
			List<GCubeGroup> vos=gCubeG.getChildren();
			for(GCubeGroup vo: vos) {
				String voScope=groupManagement.getInfrastructureScope(vo.getGroupId());
				scopes.add(voScope);
			}
		} else {
			String error="Error this portlet works only on Root Scope!";
			logger.error(error);
			throw new Exception(error);
		}
		
		logger.debug("Retrieved Scopes: {}", scopes);
		return scopes;
	}

	public void editDefinition(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		@SuppressWarnings("unused")
		UserCredentials userCredentials;

		try {
			userCredentials = SessionUtil.getUserCredentials(actionRequest);
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(), e);
			SessionErrors.add(actionRequest, "error-session");
			SessionMessages.add(actionRequest,
					PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			return;
		}

		paramsInfo(actionRequest);

		String act = ParamUtil.getString(actionRequest, "act");
		if (act == null) {
			SessionErrors.add(actionRequest, "error-edit-definition");
			SessionMessages.add(actionRequest,
					PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			return;
		}
		switch (act) {
		case "submit":
			SessionMessages.add(actionRequest, "success-definition-updated");
			actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			break;
		case "cancel":
			SessionMessages.clear(actionRequest);
			actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			break;
		default:
			SessionErrors.add(actionRequest, "error-create-definition");
			SessionMessages.add(actionRequest,
					PortalUtil.getPortletId(actionRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			actionResponse.setRenderParameter("jspPage", "/html/systemworkspaceclientdefinition/view.jsp");
			break;
		}

	}

	/*
	 * public void deleteDefinition(ActionRequest actionRequest, ActionResponse
	 * actionResponse) throws Exception {
	 * 
	 * @SuppressWarnings("unused") UserCredentials userCredentials;
	 * 
	 * try { userCredentials = SessionUtil.getUserCredentials(actionRequest); }
	 * catch (Exception e) { logger.error(e.getLocalizedMessage(), e);
	 * SessionErrors.add(actionRequest, "error-session");
	 * SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) +
	 * SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
	 * actionResponse.setRenderParameter("jspPage",
	 * "/html/systemworkspaceclientdefinition/view.jsp"); return; }
	 * 
	 * paramsInfo(actionRequest);
	 * 
	 * logger.debug("Delete Request"); String clientId =
	 * ParamUtil.getString(actionRequest, "clientId"); if (clientId == null ||
	 * clientId.isEmpty()) { logger.error("Invalid client id: " + clientId);
	 * SessionErrors.add(actionRequest, "error-invalid-clientId");
	 * SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) +
	 * SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
	 * PortalUtil.copyRequestParameters(actionRequest, actionResponse);
	 * actionResponse.setRenderParameter("jspPage",
	 * "/html/systemworkspaceclientdefinition/view.jsp"); return; }
	 * 
	 * try { DefinitionItem definitionItem = new DefinitionItem(clientId);
	 * InformationSystemUtils.deleteSD(definitionItem,
	 * Constants.SYSTEM_WORKSPACE_CLIENT_CATEGORY,
	 * userCredentials.getCurrentScope()); publishDeleteSDEvent(definitionItem);
	 * SessionMessages.add(actionRequest, "success-definition-deleted");
	 * actionResponse.setRenderParameter("jspPage",
	 * "/html/systemworkspaceclientdefinition/view.jsp");
	 * 
	 * } catch (Exception e) { logger.error(e.getLocalizedMessage(), e);
	 * SessionErrors.add(actionRequest, "error-delete-definition");
	 * SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) +
	 * SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
	 * actionResponse.setRenderParameter("jspPage",
	 * "/html/systemworkspaceclientdefinition/view.jsp"); }
	 * 
	 * }
	 */

	private void publishCreateSDEvent(DefinitionItem definitionItem) throws Exception {
		logger.debug("Trying to send the event to create System Definition: {}",definitionItem);
		CreateSDEvent toCreate = new CreateSDEvent(definitionItem);
		toCreate.publisher.publish(toCreate);
		logger.info("Done send create event for: {}", toCreate);
		try {
			Thread.sleep(Constants.SLEEP_TIME);
		} catch (InterruptedException e) {

		}
	}

	private void publishDeleteSDEvent(DefinitionItem definitionItem) {
		logger.debug("Trying to send the event to delete System Service");
		DeleteSDEvent toDelete = new DeleteSDEvent(definitionItem);
		toDelete.publisher.publish(toDelete);
		logger.info("Done send delete event " + toDelete.getName() + " for " + toDelete.getUser());
		try {
			Thread.sleep(Constants.SLEEP_TIME);
		} catch (InterruptedException e) {

		}
	}


}

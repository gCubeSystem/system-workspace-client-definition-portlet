<%@include file="init.jsp"%>
<portlet:actionURL name="createDefinition" var="createDefinitionURL" />

<liferay-ui:error key="error-invalid-clientId"
	message="Invalid Client Id!" />
<liferay-ui:error key="error-invalid-clientId-space"
	message="Invalid Client Id! Space characters are not allowed." />
<liferay-ui:error key="error-invalid-description"
	message="Invalid Description!" />


<form id="createDefinitionForm"
	action="<%=createDefinitionURL.toString()%>" method="POST"
	name="createDefinitionForm">
	<div>
		<label for="clientId">Client Id:</label> <input type="text"
			id="clientId" name="clientId" size="45"
			value="<%=ParamUtil.getString(renderRequest, "clientId")%>">
	</div>
	<div>
		<label for="description">Description:</label> <input type="text"
			id="description" name="description" size="45"
			value="<%=ParamUtil.getString(renderRequest, "description")%>">
	</div>
	<input type="hidden" id="act" name="act" value="">
	<div>
		<button class="btn btn-primary"  
			 onclick="createDefinition(this)">Save</button>
		<button class="btn btn-default"
			onclick="cancel()">Cancel</button>
	</div>
	
</form>

<%@include file="sd_dialog_progressbar.jsp" %>

<script type="text/javascript">
function cancel(){
	document.getElementById("act").value="cancel";
	location.href = '<%=createDefinitionURL.toString()%>';
}

function createDefinition(elem){
	elem.disabled=true;
    document.getElementById("act").value="submit";
    let frm=document.getElementById("createDefinitionForm");
    frm.submit();
    progressDialog();
}

</script>

<%@include file="init.jsp"%>

<portlet:renderURL var="sdDialogCreateContent"
	windowState="<%=LiferayWindowState.EXCLUSIVE.toString()%>">
	<portlet:param name="mvcPath"
		value="/html/systemworkspaceclientdefinition/sd_dialog_create_content.jsp" />
	<portlet:param name="message" value="Hello welcome" />
</portlet:renderURL>



<div>
	<aui:button name="sd_dialog_create" id="sd_dialog_create"
		value="Create">
	</aui:button>
</div>
<aui:script>
AUI().use('aui-base',
	'aui-io-plugin-deprecated',
	'liferay-util-window', 
	function(A) {
		A.one('#<portlet:namespace />sd_dialog_create').on('click', function(event){ 
			var popUpWindow=Liferay.Util.Window.getWindow(
						{
							dialog: {
								centered: true,
								constrain2view: true,
								//cssClass: 'yourCSSclassName',
								modal: true,
								resizable: false,
								width: 475
							}
						}
					).plug(
						A.Plugin.IO,
						{
							autoLoad: false
						}).render();
			popUpWindow.show();
			popUpWindow.titleNode.html("System Workspace Client Definition Dialog");
			popUpWindow.io.set('uri','<%=sdDialogCreateContent%>');
			popUpWindow.io.start();
		});
	});
</aui:script>

<%@include file="init.jsp"%>
<%@ page import="org.gcube.portlets.admin.systemworkspaceclientdefinition.OperationType" %>
<portlet:resourceURL id="tableInteraction" var="tableInteractionURL">
	<portlet:param name="operationType" value="<%=OperationType.TableInteraction.name()%>" />
</portlet:resourceURL>

<portlet:renderURL var="sdCreateURL">
    <portlet:param name="mvcPath" value="/html/systemworkspaceclientdefinition/sd_create.jsp" />
</portlet:renderURL>

<portlet:renderURL var="sdEditURL">
    <portlet:param name="mvcPath" value="/html/systemworkspaceclientdefinition/sd_edit.jsp" />
    <portlet:param name="clientId" value="clientIdValue" />
	<portlet:param name="description" value="descriptionValue" />
</portlet:renderURL>

<%--
<portlet:actionURL name="deleteDefinition" var="sdDeleteURL" >
	<portlet:param name="clientId" value="clientIdValue" />
	<portlet:param name="description" value="descriptionValue" />
</portlet:actionURL>
--%>

<% 
pageContext.setAttribute("currentGroup", GroupLocalServiceUtil.getGroup(PortalUtil.getScopeGroupId(request)));
String currentUsername = PortalUtil.getUser(request).getScreenName();
pageContext.setAttribute("currentUsername", currentUsername);
%>	


<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/datatables.min.css">
<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/buttons.dataTables.min.css">

<script src="<%=renderRequest.getContextPath()%>/js/datatables.min.js"></script>
<script src="<%=renderRequest.getContextPath()%>/js/dataTables.buttons.min.js"></script>
<script src="<%=renderRequest.getContextPath()%>/js/buttons.html5.min.js"></script>
<script src="<%=renderRequest.getContextPath()%>/js/buttons.print.min.js"></script>
 
<liferay-ui:error key="error-session" message="Invalid Session please refresh the page!" />
<liferay-ui:error key="error-create-definition" message="Error creating the definition!" />
<liferay-ui:success key="success-definition-created" message="Definition created!" />
<liferay-ui:error key="error-edit-definition" message="Error updating the definition!" />
<liferay-ui:success key="success-definition-updated" message="Definition updated!" />
<liferay-ui:error key="error-delete-definition" message="Error deleting the definition!" />
<liferay-ui:success key="success-definition-deleted" message="Definition deleted!" />

 <p>Use this portlet to automatically create system workspace client, our internal services that carry out "system" operations with the need to be authorised to access other clients with every active contexts of the infrastructure.</p>

<table id="definitionsTable" style="width:100%">
	<thead>
		<tr>
			<th>ClientId</th>
			<th>Description</th>
		</tr>
	</thead>
</table>

<%@include file="sd_dialog_progressbar.jsp" %>

<!-- Script -->
<script>

//avoid warning messages 
$.fn.dataTable.ext.errMode = 'none';


function loadItemsListIntoTable() {
	var table = $('#definitionsTable').DataTable();
	table.ajax.url('<%=tableInteractionURL%>').load();
	$('#definitionsTable_info').show();
	$('#definitionsTable_length').show();
}

function createSD(){
	location.href = '<%=sdCreateURL.toString()%>';
}

function editSD(){
	var table = $('#definitionsTable').DataTable();
	var rowSelected=table.rows('.selected').data();
	if(!("0" in rowSelected)){// true if "0" doesn't exist in object
		console.log("Please, select a row!");
		alert("Please, select a row!");
		return;
	}
	
	var selected = JSON.stringify(rowSelected["0"]);
    console.log("Selected: ",selected);
	alert(selected);
	var editUrl='<%=sdEditURL.toString()%>';
	editUrl= editUrl.replace("clientIdValue",rowSelected["0"]["ClientId"]);
	editUrl= editUrl.replace("descriptionValue",rowSelected["0"]["Description"]);
	location.href = editUrl;
	
}

<%--
function deleteSD(){
	var table = $('#definitionsTable').DataTable();
	var rowSelected=table.rows('.selected').data();
	if(!("0" in rowSelected)){// true if "0" doesn't exist in object
		console.log("Please, select a row!");
		alert("Please, select a row!");
		return;
	}
	
	var selected = rowSelected["0"]["ClientId"];
    console.log("Selected: ",selected);
    
    var confirmed = confirm("Are you sure to delete it?\n"+selected);
    if (confirmed == false) {
    	return;
    }
    
    progressDialog();
    
        
	var deleteUrl='<%=sdDeleteURL.toString()%>';
	deleteUrl= deleteUrl.replace("clientIdValue",rowSelected["0"]["ClientId"]);
	deleteUrl= deleteUrl.replace("descriptionValue",rowSelected["0"]["Description"]);
	location.href = deleteUrl;
	
}
--%>

function refreshSD(){
	var table = $('#definitionsTable').DataTable();
	table.ajax.reload();
}




$.fn.dataTable.ext.buttons.createSD = {
	    text: '<span class="material-icons">add_circle_outline</span><span class="circle_button_text">Create</span>',
	    action: function ( e, dt, node, config ) {
	        createSD();
	    },
	    attr:  {
            title: 'Create',
            'class': 'btn circle_button'
        }
};

<%--
$.fn.dataTable.ext.buttons.deleteSD = {
	    text: '<span class="material-icons">remove_circle_outline</span><span class="circle_button_text">Delete</span>',
	    action: function ( e, dt, node, config ) {
	        deleteSD();
	    },
		attr:  {
    		title: 'Delete',
    		'class': 'btn circle_button'
		}
};
--%>

$.fn.dataTable.ext.buttons.refreshSD = {
	    text: '<span class="material-icons">refresh</span><span class="circle_button_text">Refresh</span>',
	    action: function ( e, dt, node, config ) {
	    	refreshSD();
	    },
		attr:  {
    		title: 'Refresh',
    		'class': 'btn circle_button'
		}
};



//Instance of DataTables framework, see https://datatables.net/manual/
function mainTable() {
	var table = $('#definitionsTable').DataTable( {
		retrieve: true, // tell DataTables that you are aware that the initialisation options can't be changed after initialisation, and that should that occur, that you just want the DataTable instance to be returned.
		"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
		"dom": "<'row'<'small-6 columns'Bf>r>t<'row'<'mydt-pagination'p><'#mydtwrap'<'mydt-block'l><i>>'>",
		select: {
	         style: 'single'
	    },
	    buttons: [
	        'createSD','refreshSD'
	    ],
		processing: true,
		oLanguage: {sProcessing: "<div></div>"},
		serverSide: true, 
		searching: true,
		ordering:  true,
		
		"order": [],
		search: {
            "return": true
        },
		"language": {
		      "emptyTable": "Empty",
		      "info": "_START_ to _END_ of _TOTAL_ items"
		},
		"stripeClasses": [ 'strip1', 'strip2'],
	    "ajax": {
	      "url":"<%=tableInteractionURL%>",
	      "dataSrc": function ( json ) { //here is the json return by the ajax call
	    	  console.log("JSON table:", json);
	    	  <%-- $( "div.messages-container" ).html(json.messages); --%>
	          return json.data;
	        }
	     },
	    "columns": [
	        { "data": "ClientId",
	          "render": function ( data, type, row, meta ) {
	        	return data;
	          }
	        },
	        { "data": "Description",
	          "render": function ( data, type, row, meta ) {
					return data;
	          }
	        }
	    ]
		} 
	);
	
	/*
	new $.fn.dataTable.Buttons( table, {
	    "buttons": [
	        "copy", "excel", "pdf"
	    ]
	} );
	 
	table.buttons().container()
	    .appendTo( $('.small-6.columns:eq(0)', table.table().container() ) );
    */
	
	table
	.on( 'error.dt', function ( e, settings, techNote, message ) {
		$('div.messages-container').text('Ops, cannot reach the server. Please try to reload the page or check your internet connection');
		console.log( 'An error has happened in the server: ', message );
	} );
	
	table.on( 'click', 'tr', function () {
	        if ( $(this).hasClass('selected') ) {
	            $(this).removeClass('selected');
	            $('#sdEdit').disabled=true;
	            //$('#sdDelete').disabled=true;
	        } else {
	            table.$('tr.selected').removeClass('selected');
	            $(this).addClass('selected');
	            $('#sdEdit').disabled=false;
	            //$('#sdDelete').disabled=false;

	        }
	} );
	 
	<%-- Remove selected by a button
	 $('#button').click( function () {
	        table.row('.selected').remove().draw( false );
	    } );
	--%>
	
	
	<%-- In alternative a simple loader image use:
	  oLanguage: {sProcessing: "<div id='loader'></div>"}
	--%>
	table.on('processing.dt', function ( e, settings, processing ) {
		$('#definitionsTable_processing').css( 'display', 'none');
		if(processing){
	    	progressDialog();
	    } else {
	    	progressDialogClose();
	    }
				
		<%-- $('#definitionsTable_processing').css( 'display', processing ? 'block' : 'none' ); --%>
	} );
		
	console.log("mainTable() done");
}


$(document).ready(mainTable);

</script>



<%@include file="init.jsp"%>

<portlet:renderURL var="sdSimpleProgressBar"
	windowState="<%=LiferayWindowState.EXCLUSIVE.toString()%>">
	<portlet:param name="mvcPath"
		value="/html/systemworkspaceclientdefinition/simpleProgressBar.jsp" />
</portlet:renderURL>

<script>
var popUpWindowProgress;

function progressDialog(){
  AUI().use('aui-base',
	'aui-io-plugin-deprecated',
	'liferay-util-window', 
	function(A) {
		window.popUpWindowProgress=Liferay.Util.Window.getWindow(
						{
							dialog: {
								centered: true,
								constrain2view: true,
								cssClass: 'progressDialog',
								modal: true,
								resizable: false,
								width: 320,
							    height: 280,
							    destroyOnClose: true,
							    destroyOnHide: true,
							    toolbars: {
								}
							}
						}
			).plug(
				A.Plugin.IO,
				{
					autoLoad: false
				}
			).render();
		window.popUpWindowProgress.show();
		window.popUpWindowProgress.titleNode.html("Processing...");
		window.popUpWindowProgress.bodyNode.html("<div id='loader'></div>");
		
	});
}


<%--
'<%=ssdSimpleProgressBar%>'
window.popUpWindowProgress.io.set("uri","<%=ssdSimpleProgressBar%>");
window.popUpWindowProgress.io.start();
		
window.popUpWindowProgress.bodyNode.html("<%@include file="simpleProgressBar.jsp" %>");
--%>


function progressDialogClose(){
	if(window.popUpWindowProgress){
		console.log("<%=sdSimpleProgressBar%>");
		console.log(window.popUpWindowProgress);
		window.popUpWindowProgress.destroy();
	}
	<%--window.Liferay.Util.getOpener().closePopup();--%>
}
</script>

<%--
footer: [
										{
											label: '&lt;%= UnicodeLanguageUtil.get(pageContext, "cancel") %&gt;',
											on: {
												click: function() {
													messagingDialog.hide();
												}
											}
										}
									]
 --%>
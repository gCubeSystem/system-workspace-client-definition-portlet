
<div id="simpleProgress">
	<div id="simpleBar"></div>
</div>

<script type="text/javascript">
	var elem = document.getElementById("simpleBar");

	var position = 0;
	var rm = true;
	var id;

	var start;

	function frame() {
		if (start === undefined){
			start = new Date();
		} 
		let timestamp = new Date();	
		const elapsed = timestamp - start;
		
		if (rm) {
			if (position > 69) {
				rm = false;
				position=position-(0.1*elapsed);
			} else {
				position=position+(0.1*elapsed);
			}
		} else {
			if (position > 0) {
				position=position-(0.1*elapsed);
			} else {
				rm = true;
				position=position+(0.1*elapsed);
			}
		}
		
		elem.style.marginLeft = position + "%";
		start=timestamp;
		//id = window.requestAnimationFrame(frame)
	}

	function end() {
		//window.cancelAnimationFrame(id);
		clearInterval(id);
		var elemContainer = document.getElementById("simpleProgress");
		//elemContainer.style.display = "none";
		elemContainer.style.visibility = "hidden";
	}

	function move() {
		var elemContainer = document.getElementById("simpleProgress");
		//elemContainer.style.display = "block";
		elemContainer.style.visibility = "visible";
		//id = window.requestAnimationFrame(frame);
		id=setInterval(frame, 100);
	}

	function loadingActive() {
		console.log("Loading...");
		move();
	}

	loadingActive();
</script>


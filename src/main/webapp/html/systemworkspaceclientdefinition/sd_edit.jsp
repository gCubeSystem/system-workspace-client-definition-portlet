<%@include file="init.jsp"%>
<portlet:actionURL name="editDefinition" var="editDefinitionURL" />

<form id="editDefinitionForm"
	action="<%=editDefinitionURL.toString()%>" method="POST"
	name="editDefinitionForm">
	<div>
		<label for="clientId">Client Id:</label>
		<input type="text" id="clientId" name="clientId" size="45" 
		value="<%=ParamUtil.getString(renderRequest, "clientId")%>">
	</div>
	<div>
		<label for="description">Description:</label> 
		<input type="text" id="description" name="description" size="45"
		value="<%=ParamUtil.getString(renderRequest, "description")%>">
	</div>
	<div>
		<button class="btn btn-primary" type="submit" name="act"
			value="submit">Save</button>
		<button class="btn btn-default"
			onClick="location.href = '<%=editDefinitionURL.toString()%>'"
			name="act" value="cancel">Cancel</button>
	</div>
</form>


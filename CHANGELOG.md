This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "system-workspace-client-definition-portlet"


## [v1.0.0] - 2023-07-12

### Fixes

- First Release [#22514]